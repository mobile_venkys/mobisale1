Ext.define("venkys_sale.view.OverseaspanelContainer",{
	extend: "Ext.Panel",
	xtype: "overseaspanelcontainer",
    requires: ["venkys_sale.view.OverseasPanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Overseas",
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'blistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
			
				
			]
		};

		this.add([toolbar, { xtype: "overseaspanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Overseas Plant",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});