Ext.define("venkys_sale.view.BiscuitList",{
//extend : 'Ext.ux.touch.grid.View',
extend : 'Ext.ux.touch.grid.List',
 xtype: "biscuitlist",
  requires : [
        'Ext.ux.touch.grid.feature.Feature',
        'Ext.ux.touch.grid.feature.Editable',
        'Ext.ux.touch.grid.feature.Sorter',
        'Ext.field.Number',
        'venkys_sale.store.Cattlefeed'
    ],
    //fullscreen: true,
 config: {
    store: "BiscuitList",
	
    columns  : [
        
			{
                header    : 'Details',
                dataIndex : 'Details',
                width     : '50%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'textfield'
                }
            },			
			{
                header    : 'Result',
                dataIndex : 'Result',
                width     : '50%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            }
            
        ],
        features : [
            /*{
                ftype    : 'Ext.ux.touch.grid.feature.Sorter',
                launchFn : 'initialize'
            },*/
            {
                ftype    : 'Ext.ux.touch.grid.feature.Editable',
                launchFn : 'initialize'
            }
        ]
    
	},
});