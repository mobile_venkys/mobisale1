/**
 * The main application viewport, which displays the whole application
 * @extends Ext.Viewport
 */
Ext.define('venkys_sale.view.DynamicGrid', {
    extend: 'Ext.Viewport',    
    layout: 'fit',

    requires: [
        'Ext.ux.grid.DynamicGrid'
    ],
    
    initComponent: function() {
        console.log('Viewport initComponent!');

        var me = this;
        
        Ext.apply(me, {
            items: [
                {
                    xtype: 'dynamicGrid',
                    //url: './data.js',
					//url:'http://192.168.13.52:36/m/sale1/app/php/XpressList.php',
                      url:'http://127.0.0.1/application/Grid-Display/app/php/XpressList.php',
					  url:'http://127.0.0.1/application/Grid-Display/app/php/xpress_details.json',
					//url: 'http://192.168.13.52:36/m/sale1/app/php/xpress_details.json',
					dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'top',
                            items: [
                                {
                                    xtype: 'button',
                                    text: 'Load Data',
                                    action: 'btnLoadData'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Load Data2',
                                    action: 'btnLoadData2'
                                }
                            ]
                        }
                    ]
                }
            ]
        });
                
        me.callParent(arguments);
    }
});
