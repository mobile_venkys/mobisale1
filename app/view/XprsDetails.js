Ext.define("venkys_sale.view.XprsDetails",{
extend : 'Ext.ux.touch.grid.List',
 xtype: "xprsdetails",
 requires : [
        'Ext.ux.touch.grid.feature.Feature',
        'Ext.ux.touch.grid.feature.Editable',
        'Ext.ux.touch.grid.feature.Sorter',
        'Ext.field.Number',
        'venkys_sale.store.XprsDetails'
    ],

    config : {
       
		store: 'XprsDetails',
	    //url:"http://127.0.0.1/application/Grid-Display/app/php/xpress_details.json",
        //store    : true,
		//url :'http://127.0.0.1/backbone/XpressList.php',
        columns  : [
           {
                header    : 'Outlet',
                dataIndex : 'Outlet',
                width     : '25%',
                style    : "text-align: left;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Today',
                dataIndex : 'Today',
                width     : '25%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Yesterday',
                dataIndex : 'Yesterday',
                width     : '25%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Monthly',
                dataIndex : 'Monthly',
                width     : '25%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            }
			
			        
        ],
        features : [
            /*{
                ftype    : 'Ext.ux.touch.grid.feature.Sorter',
                launchFn : 'initialize'
            },*/
            {
                ftype    : 'Ext.ux.touch.grid.feature.Editable',
                launchFn : 'initialize'
            }
        ]
    },

	
});