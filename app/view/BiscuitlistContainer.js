Ext.define("venkys_sale.view.BiscuitlistContainer",{
	extend: "Ext.Panel",
	xtype: "biscuitlistcontainer",
    requires: ["venkys_sale.view.BiscuitList"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Contacts",
			id: 'myTitleBisc',
			
			items: [
			
			
				{
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'bisBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'refreshbiscuittap',
					
				},
				
				
			]
		};
		var footerbar = {
			xtype: "toolbar",
			docked: "bottom",
			//title: "Cattle Details",
			//id: 'myTitleC',
			
			
			items: [
			
			
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "index", 
					action: 'blistBack',
					//iconCls: 'reply',
                    //iconMask: true,
				},
				
				
			]
		};

		this.add([toolbar,footerbar, { xtype: "biscuitlist"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		//fullscreen: true,
		title: "Contacts",
		iconCls: "address_book",
		iconMask: true
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});