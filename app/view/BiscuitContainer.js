Ext.define("venkys_sale.view.BiscuitContainer",{
	extend: "Ext.Panel",
	xtype: "biscuitcontainer",
    requires: ["venkys_sale.view.BiscuitPanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Biscuit Plants",
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'blistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				
			]
		};

		this.add([toolbar, { xtype: "biscuitpanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Cattle Feed",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});