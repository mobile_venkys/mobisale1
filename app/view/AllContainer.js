Ext.define("venkys_sale.view.AllContainer",{
	extend: "Ext.Panel",
	xtype: "allcontainer",
    requires: ["venkys_sale.view.AllPanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Plants",
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'blistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
			
				
			]
		};

		this.add([toolbar, { xtype: "allpanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Plants",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});