Ext.define("venkys_sale.view.DivisionContainer",{
	extend: "Ext.Panel",
	xtype: "divisioncontainer",
    requires: ["venkys_sale.view.DivisionPanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Divisions",
			
			
			items: [
			
			
				{ xtype: "spacer" },
				
				
			]
		};

		this.add([toolbar, { xtype: "divisionpanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		//fullscreen: true,
		title: "Home",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});