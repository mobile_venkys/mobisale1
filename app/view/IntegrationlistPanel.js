Ext.define("venkys_sale.view.IntegrationlistPanel",{
	extend: "Ext.Panel",
	xtype: "integrationlistpanel",
    requires: ["venkys_sale.view.IntegrationList"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Contacts",
			id: 'myTitleInte',
			
			items: [
			
			
				{
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'inteback',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				
				{
					xtype: "button",
					ui: "home",
					text: "index", 
					action: 'blistBack',
					//iconCls: 'reply',
                    //iconMask: true,
				},
				
				
			]
		};

		this.add([toolbar, { xtype: "integrationlist"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		//fullscreen: true,
		title: "Contacts",
		iconCls: "address_book",
		iconMask: true
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});