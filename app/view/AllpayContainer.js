Ext.define("venkys_sale.view.AllpayContainer",{
	extend: "Ext.Panel",
	xtype: "allpaycontainer",
    requires: ["venkys_sale.view.AllpayPanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Plants",
			id: 'allpayid',
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'allcollBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'refreshallpaytap',
					//iconCls: 'reply',
                   // iconMask: true,
				},
				
			
				
			]
		};

		
		var footerbar = {
			xtype: "toolbar",
			docked: "bottom",
			//title: "Plants",
			//id: 'allcollectionid',
			
			
			items: [
			   {
					xtype: "button",
					text: "index", 
					action: 'blistBack',
				},
			
				{ xtype: "spacer" },
				
			
				
			]
		};
		this.add([toolbar,footerbar, { xtype: "allpaypanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Plants",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});