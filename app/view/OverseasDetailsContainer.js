Ext.define("venkys_sale.view.OverseasDetailsContainer",{
	extend: "Ext.Panel",
	xtype: "overseasdetailscontainer",
    requires: ["venkys_sale.view.OverseasDetails"],
	

	initialize: function() {
	
		var toolbar = {
		    store:"All",
			xtype: "toolbar",
			docked: "top",
			title: "Overseas Plants",
			//id: 'myTitle',
			
			
			
			items: [
			{
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'overBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'onrefreshovertap',
					//iconCls: 'reply',
                   // iconMask: true,
				},
				
				
			]
		};

		this.add([toolbar, { xtype: "overseasdetails"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		//title: "Cattle Feed",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});