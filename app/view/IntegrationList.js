Ext.define("venkys_sale.view.IntegrationList",{
//extend : 'Ext.ux.touch.grid.View',
 extend : 'Ext.ux.touch.grid.List',
 xtype: "integrationlist",
  requires : [
        'Ext.ux.touch.grid.feature.Feature',
        'Ext.ux.touch.grid.feature.Editable',
        'Ext.ux.touch.grid.feature.Sorter',
        'Ext.field.Number',
        'venkys_sale.store.IntegrationList'
    ],
    //fullscreen: true,
config: {
    store: "IntegrationList",

      columns  : [
        
			{
                header    : 'Company',
                dataIndex : 'Company',
                width     : '25%',
                style    : "text-align: left;",
				editor    : {
                    xtype  : 'textfield'
                }
            },			
			{
                header    : 'Yesterday',
                dataIndex : 'Yesterday',
                width     : '25%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Day_b4yes',
                dataIndex : 'Byesterday',
                width     : '25%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : '2days_b4yes',
                dataIndex : 'Bbyesterday',
                width     : '25%',
                style    : "text-align: center;",
				/*renderer : function(val) {
					var color = (val > 0) ? "00FF00" : "FF0000";
					return "<span style='color: #" + color + ";'>" + val + "</span>";
				},*/
				editor    : {
                    xtype  : 'numberfield'
                }
            }
            
        ],
        features : [
            /*{
                ftype    : 'Ext.ux.touch.grid.feature.Sorter',
                launchFn : 'initialize'
            },*/
            {
                ftype    : 'Ext.ux.touch.grid.feature.Editable',
                launchFn : 'initialize'
            }
        ]
	},
});