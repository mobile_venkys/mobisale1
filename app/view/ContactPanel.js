Ext.define("venkys_sale.view.ContactPanel",{
	extend: "Ext.Panel",
	xtype: "contactpanel",
    requires: ["venkys_sale.view.Contact"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Contacts",
			
			
			items: [
			
			
				{ xtype: "spacer" },
				
				
			]
		};

		this.add([toolbar, { xtype: "contact"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		//fullscreen: true,
		title: "Contacts",
		iconCls: "address_book",
		iconMask: true
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});