Ext.define("venkys_sale.view.Contact",{
extend: "Ext.DataView",
 xtype: "contact",
    //fullscreen: true,
config: {
    store: "SaleDetails",

    itemTpl:[
	'<div style="position: relative;overflow: auto; padding-left: 10px;padding-right: 10px; padding-top: 30px; margin-top: -1px;">',
	'<div style="overflow: auto; ">',
					'<div style="background-color:Green;overflow: auto;float: left; width: 200px; color:Blue">Today\'s Collection</div>',
		            '<div style="width: 200px;background-color:#336699;overflow: auto;float: left; ">&nbsp;&nbsp;{todays_amt}&nbsp;&nbsp;Rs.</div>',
    '<br style="clear: left;" />',
				    '</div>' ,
	
	'<div style="overflow: auto;">',
					'<div style="width: 200px; background-color:Green;overflow: auto;float: left; color:Blue">Yesterday\'s Collection</div>',
		            '<div style="width: 200px;background-color:#336699;overflow: auto;float: left;">&nbsp;&nbsp;{yes_amt}&nbsp;&nbsp;Rs.</div>',
    '<br style="clear: left;" />',
				    '</div>' ,
					'<div style="overflow: auto;">',
					'<div style="width: 200px; background-color:red;overflow: auto;float: left; color:Blue">Avg.Monthly Collection</div>',
		            '<div style="width: 200px;background-color:#336699;overflow: auto;float: left;">&nbsp;&nbsp;{mnthly_coll}&nbsp;&nbsp;Rs.</div>',
    '<br style="clear: left;" />',
				    '</div>' ,

              '<div style="overflow: auto;">',
					'<div style="width: 200px; background-color:red;overflow: auto;float: left; color:Blue">Target Collection</div>',
		            '<div style="width: 200px;background-color:#336699;overflow: auto;float: left;">&nbsp;&nbsp;{tar_sale}&nbsp;&nbsp;Rs.</div>',
    '<br style="clear: left;" />',
				    '</div>' ,	
					 '</div>' ,
	],
	},
});