Ext.define("venkys_sale.view.XprsContainer",{
	extend: "Ext.Panel",
	xtype: "xprscontainer",
    requires: ["venkys_sale.view.XprsDetails"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Outlet Details",
			//id: 'myTitleC',
			
			items: [
			{
					xtype: "button",
					//ui: "back",
					//text: "index", 
					action: 'blistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'refreshxprstap',
					//iconCls: 'reply',
                   // iconMask: true,
				},
				
				
			]
		};
		var footerbar = {
			xtype: "toolbar",
			docked: "bottom",
			//title: "Cattle Details",
			//id: 'myTitleC',
			
			
			items: [
			
			
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "index", 
					action: 'blistBack',
					//iconCls: 'reply',
                    //iconMask: true,
				},
				
				
			]
		};
		
		this.add([toolbar,footerbar, { xtype: "xprsdetails"}
		
		]);
		

	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		//title: "Cattle Feed",
		iconCls: "address_book",
		iconMask: true
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});