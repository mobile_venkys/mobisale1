Ext.define("venkys_sale.view.SaleDetail",{
	//extend: "Ext.form.Panel",
   extend: "Ext.dataview.List",
   xtype: "saledetail",
   requires: ["Ext.plugin.ListPaging"],
	//store: "Note",
   // itemId:"loginpanel",

	config: {
	store: {
        fields: ['name'],
        data: [
            {name: 'Sale Today'},
            {name: 'Monthly Sale'},
			{name: 'Till Now'},
			{name: 'Average'},
			{name: 'Target'}
            
        ]
    },
		
           plugins: [
			        {
						xclass: "Ext.plugin.ListPaging",
						autoPaging: false
			        }
		            ],
		 
		itemTpl: '{name}',
		
		onItemDisclosure: function(record,btn,index) 
		{
              this.fireEvent("editBussinessPartnerCommand", record);
		}
		},
	
	
});