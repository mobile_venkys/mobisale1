Ext.define("venkys_sale.view.LiveSale",{
//extend : 'Ext.ux.touch.grid.View',
extend : 'Ext.ux.touch.grid.List',
 xtype: "livesale",
 requires : [
        'Ext.ux.touch.grid.feature.Feature',
        'Ext.ux.touch.grid.feature.Editable',
        'Ext.ux.touch.grid.feature.Sorter',
        'Ext.field.Number',
        'venkys_sale.store.Live_sale'
    ], 
 
 //store: "Live_sale",
 
    //fullscreen: true,
config: {
    store: "Live_sale",
	
    columns  : [
           {
                header    : 'Plants',
                dataIndex : 'Plants',
                width     : '50%',
                style    : "text-align: left;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },			
			{
                header    : 'Live',
                dataIndex : 'Today',
                width     : '50%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            }
			
			        
        ],
        features : [
            /*{
                ftype    : 'Ext.ux.touch.grid.feature.Sorter',
                launchFn : 'initialize'
            },*/
            {
                ftype    : 'Ext.ux.touch.grid.feature.Editable',
                launchFn : 'initialize'
            }
        ]
    
	},
});