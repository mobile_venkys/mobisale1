Ext.define("venkys_sale.view.LivesaleContainer",{
	extend: "Ext.Panel",
	xtype: "livesalecontainer",
    requires: ["venkys_sale.view.LiveSale"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Plants",
			id: 'alllivesaleid',
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'allcollBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'refreshlivesaletap',
					//iconCls: 'reply',
                   // iconMask: true,
				},
				
			]
		};
		
		var footerbar = {
			xtype: "toolbar",
			docked: "bottom",
			//title: "Plants",
			//id: 'allcollectionid',
			
			
			items: [
			   {
					xtype: "button",
					text: "index", 
					action: 'blistBack',
				},
			
				{ xtype: "spacer" },
				
			
				
			]
		};

		this.add([toolbar,footerbar, { xtype: "livesale"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Plants",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});