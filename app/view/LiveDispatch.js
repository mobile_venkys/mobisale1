Ext.define("venkys_sale.view.LiveDispatch",{
//extend : 'Ext.ux.touch.grid.View',
 extend : 'Ext.ux.touch.grid.List',
 xtype: "livedispatch",
 requires : [
        'Ext.ux.touch.grid.feature.Feature',
        'Ext.ux.touch.grid.feature.Editable',
        'Ext.ux.touch.grid.feature.Sorter',
        'Ext.field.Number',
        'venkys_sale.store.Live_dispatch'
    ],	
 //store: "Live_dispatch",
    //fullscreen: true,
	
	
config: {
    store: "Live_dispatch",
    columns  : [
           {
                header    : 'Plants',
                dataIndex : 'Plants',
                width     : '50%',
                style    : "text-align: left;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },			
			{
                header    : 'Live',
                dataIndex : 'Today',
                width     : '50%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            }
			
			        
        ],
        features : [
            /*{
                ftype    : 'Ext.ux.touch.grid.feature.Sorter',
                launchFn : 'initialize'
            },*/
            {
                ftype    : 'Ext.ux.touch.grid.feature.Editable',
                launchFn : 'initialize'
            }
        ]
    
	},
});