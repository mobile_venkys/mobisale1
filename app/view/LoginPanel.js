Ext.define("venkys_sale.view.LoginPanel",{
	//extend: "Ext.form.Panel",
   extend: "Ext.dataview.List",
   xtype: "loginpanel",
   requires: ["Ext.plugin.ListPaging"],
	//store: "Note",
   // itemId:"loginpanel",

	config: {
	store: {
        fields: ['name'],
        data: [
            {name: 'Khanna'},
            {name: 'Ketkawale'}
            
        ]
    },
		
           plugins: [
			        {
						xclass: "Ext.plugin.ListPaging",
						autoPaging: false
			        }
		            ],
		 
		itemTpl: '{name}',
		
		onItemDisclosure: function(record,btn,index) 
		{
              this.fireEvent("editBussinessPartnerCommand", record);
		}
		},
	
	
});