Ext.define("venkys_sale.view.AllPanel",{
	//extend: "Ext.form.Panel",
   extend: "Ext.dataview.List",
   xtype: "allpanel",
   requires: ["Ext.plugin.ListPaging"],
	//store: "Plant",
   // itemId:"loginpanel",

	config: {
	
	 store: {
        fields: ['name'],
        data: [
           /* {name: 'Sale'},*/
            {name: 'Dispatch'},
            {name: 'Collection'},
            {name: 'Payment'},
			 {name: 'Live Sale'},
			  {name: 'Live Dispatch'}
        ]
    },
		
		
          plugins: [
			        {
						xclass: "Ext.plugin.ListPaging",
						autoPaging: false
			        }
		            ],
		 
		itemTpl:  [
		'{name}',
		
		
		
		
		],
				  
		
		/*onItemDisclosure: function(record,btn,index) 
		{
		//alert("Hiii");
              this.fireEvent("SaleCommand", record,index);
		}*/
		},
	
	
});