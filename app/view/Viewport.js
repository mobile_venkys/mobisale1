Ext.define("venkys_sale.view.Viewport",{
	extend: "Ext.Panel",
initialize: function() {},
config: {
	    
		fullscreen: true,
		layout: "card",
		items: [{xtype: "mainpanel"},{xtype: "divisioncontainer"},{xtype: "divisionpanel"},{xtype: "plantpanelcontainer" } ,{xtype: "plantpanel"},{xtype: "saledetails"},{xtype: "saledetailscontainer"},{xtype: "cattlepanel"},{xtype: "cattlecontainer"},{xtype: "integrationpanel"},{xtype: "integrationcontainer"},{xtype: "hatcherypanel"},{xtype: "hatcherycontainer"},{xtype: "biscuitpanel"},{xtype: "biscuitcontainer"},{xtype: "cattlefeed"},{xtype: "cattlefeedcontainer"},/*{xtype: "contact"},{xtype: "contactpanel"},*/{xtype: "hatcherylist"},{xtype: "hatcherylistcontainer"},{xtype: "integrationlist"},{xtype: "integrationlistpanel"},{xtype: "biscuitlist"},{xtype: "biscuitlistcontainer"},{xtype: "overseaspanel"},{xtype: "overseaspanelcontainer"},{xtype: "overseasdetailscontainer"},{xtype: "overseasdetails"},{xtype: "allpanel"},{xtype:"allcontainer"},{xtype: "allsalepanel"},{xtype: "allsalecontainer"},{xtype: "alldispatchpanel"},{xtype: "alldispatchcontainer"},{xtype: "allcollpanel"},{xtype: "allcollcontainer"},{xtype: "livesale"},{xtype: "livesalecontainer"},{xtype: "livedispatch"},{xtype:"livedispatchcontainer"},{xtype: "allpaypanel"},{xtype: "allpaycontainer"},{xtype: "xprscontainer"},{xtype: "xprsdetails"},{xtype: "feedcontainer"},{xtype: "feeddetails"},{xtype: "vaccinescontainer"},{xtype: "vaccinesdetails"},{xtype: "tradecontainer"},{xtype: "tradepanel"}]
	}
});