Ext.define("venkys_sale.view.AllcollPanel",{
//extend : 'Ext.ux.touch.grid.View',
extend : 'Ext.ux.touch.grid.List',
 xtype: "allcollpanel",
 requires : [
        'Ext.ux.touch.grid.feature.Feature',
        'Ext.ux.touch.grid.feature.Editable',
        'Ext.ux.touch.grid.feature.Sorter',
        'Ext.field.Number',
        'venkys_sale.store.Salesdispatch'
    ],
//store: "Coll_pay",
    //fullscreen: true,
 config : {
       
		store: 'Coll_pay',
	    //url:"http://127.0.0.1/application/Grid-Display/app/php/xpress_details.json",
        //store    : true,
		//url :'http://127.0.0.1/backbone/XpressList.php',
        columns  : [
           {
                header    : 'Plants',
                dataIndex : 'Plants',
                width     : '30%',
                style    : "text-align: left;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Yesterday',
                dataIndex : 'Yesterday',
                width     : '35%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Monthly',
                dataIndex : 'Monthly',
                width     : '35%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            }
			
			        
        ],
        features : [
            /*{
                ftype    : 'Ext.ux.touch.grid.feature.Sorter',
                launchFn : 'initialize'
            },*/
            {
                ftype    : 'Ext.ux.touch.grid.feature.Editable',
                launchFn : 'initialize'
            }
        ]
    },

});