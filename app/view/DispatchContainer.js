Ext.define("venkys_sale.view.DispatchContainer",{
	extend: "Ext.Panel",
	xtype: "dispatchcontainer",
    requires: ["venkys_sale.view.Dispatch"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Dispatch",
			
			
			items: [
			
			{
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'blistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
				{ xtype: "spacer" },
				
				
			]
		};

		this.add([toolbar, { xtype: "dispatch"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Cattle Feed",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});