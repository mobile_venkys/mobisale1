Ext.define("venkys_sale.view.SaleDetailsContainer",{
	extend: "Ext.Panel",
	xtype: "saledetailscontainer",
    requires: ["venkys_sale.view.SaleDetails"],
	

	initialize: function() {
	
		var toolbar = {
		    //store:"All",
			xtype: "toolbar",
			docked: "top",
			title: "plant_name",
			id: 'myTitle',
			
			
			
			items: [
			{
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'plantBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'refreshtap',
					//iconCls: 'reply',
                   // iconMask: true,
				},
				
				
				
			]
		};

		var toolbar_down = {
		    store:"All",
			xtype: "toolbar",
			docked: "bottom",
			//title: "plant_name",
			//id: 'myTitle',
			
			
			
			items: [
			{
					xtype: "button",
					//ui: "back",
					text: "index", 
					action: 'blistBack',
					//iconCls: 'reply',
                    //iconMask: true,
				},
		    ]
		};
		
		this.add([toolbar,toolbar_down, { xtype: "saledetails"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		//title: "Cattle Feed",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});