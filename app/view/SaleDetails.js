Ext.define("venkys_sale.view.SaleDetails",{
//extend : 'Ext.ux.touch.grid.View',
extend : 'Ext.ux.touch.grid.List',
 xtype: "saledetails",
  requires : [
        'Ext.ux.touch.grid.feature.Feature',
        'Ext.ux.touch.grid.feature.Editable',
        'Ext.ux.touch.grid.feature.Sorter',
        'Ext.field.Number',
        'venkys_sale.store.All'
    ],

    //fullscreen: true,
  config : 
       {       
		store: 'All',	    
        columns  : [
           {
                header    : 'Details',
                dataIndex : 'Details',				
                width     : '25%',
                style    : "text-align: left;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Today',
                dataIndex : 'Today',
                width     : '25%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Yesterday',
                dataIndex : 'Yesterday',
                width     : '25%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Monthly',
                dataIndex : 'Monthly',
                width     : '25%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            }
			
			        
        ],
        features : [
            /*{
                ftype    : 'Ext.ux.touch.grid.feature.Sorter',
                launchFn : 'initialize'
            },*/
            {
                ftype    : 'Ext.ux.touch.grid.feature.Editable',
                launchFn : 'initialize'
            }
        ]
    },
});