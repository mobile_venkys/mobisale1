Ext.define("venkys_sale.view.CattlefeedContainer",{
	extend: "Ext.Panel",
	xtype: "cattlefeedcontainer",
    requires: ["venkys_sale.view.CattleFeed"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Cattle Details",
			id: 'myTitleC',
			
			items: [
			{
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'cplantBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'refreshcattletap',
					//iconCls: 'reply',
                   // iconMask: true,
				},
				
				
			]
		};
		var footerbar = {
			xtype: "toolbar",
			docked: "bottom",
			//title: "Cattle Details",
			//id: 'myTitleC',
			
			
			items: [
			
			
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "index", 
					action: 'blistBack',
					//iconCls: 'reply',
                    //iconMask: true,
				},
				
				
			]
		};
		
		this.add([toolbar,footerbar, { xtype: "cattlefeed"}
		
		]);
		

	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		//title: "Cattle Feed",
		iconCls: "address_book",
		iconMask: true
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});