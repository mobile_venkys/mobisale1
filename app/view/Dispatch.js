Ext.define("venkys_sale.view.Dispatch",{
extend: "Ext.DataView",
 xtype: "dispatch",
    //fullscreen: true,
config: {
    store: "Dispatch",

    itemTpl:[
	'<div style="position: relative;overflow: auto; padding-left: 10px;padding-right: 10px; padding-top: 30px; margin-top: -1px;">',
	'<div style="overflow: auto; ">',
					'<div style="background-color:Green;overflow: auto;float: left; width: 150px; color:Blue">Today\'s Sale</div>',
		            '<div style="background-color:#336699;overflow: auto;float: left; ">&nbsp;&nbsp;{todays_qty}&nbsp;&nbsp;tons</div>',
    '<br style="clear: left;" />',
				    '</div>' ,
	
	'<div style="overflow: auto;">',
					'<div style="width: 150px; background-color:Green;overflow: auto;float: left; color:Blue">Yesterday\'s Sale</div>',
		            '<div style="background-color:#336699;overflow: auto;float: left;">&nbsp;&nbsp;{yes_qty}&nbsp;&nbsp;tons</div>',
    '<br style="clear: left;" />',
				    '</div>' ,
					 '</div>' ,
	],
	},
});