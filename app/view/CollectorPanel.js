Ext.define("venkys_sale.view.CollectorPanel",{
	extend: "Ext.Panel",
	xtype: "collectorpanel",
    requires: ["venkys_sale.view.Collector"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Collection",
			
			
			items: [
			
			{
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'blistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
				{ xtype: "spacer" },
				
				
			]
		};

		this.add([toolbar, { xtype: "collector"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Cattle Feed",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});