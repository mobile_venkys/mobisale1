Ext.define("venkys_sale.view.AlldispatchContainer",{
	extend: "Ext.Panel",
	xtype: "alldispatchcontainer",
    requires: ["venkys_sale.view.AllsalePanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Plants",
			id: 'alldispatchsaleid',
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'alldispatchBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				
				
			{ xtype: "spacer" },
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'refreshalldispatchtap',
					//iconCls: 'reply',
                   // iconMask: true,
				},
				
				
			]
		};
		
		var footerbar = {
			xtype: "toolbar",
			docked: "bottom",
			//title: "Cattle Details",
			//id: 'myTitleC',
			
			
			items: [
			
			
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "index", 
					action: 'blistBack',
					//iconCls: 'reply',
                    //iconMask: true,
				},
				
				
			]
		};

		this.add([toolbar,footerbar, { xtype: "alldispatchpanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Plants",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});