Ext.define("venkys_sale.view.VaccinesContainer",{
	extend: "Ext.Panel",
	xtype: "vaccinescontainer",
    requires: ["venkys_sale.view.VaccinesDetails"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Vaccines Details",
			id: 'vaccinesid',
			//id: 'myTitleC',
			
			items: [
			{
					xtype: "button",
					//ui: "back",
					//text: "index", 
					action: 'vaccinesBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'refreshvaccinestap',
					//iconCls: 'reply',
                   // iconMask: true,
				},
				
				
			]
		};
		var footerbar = {
			xtype: "toolbar",
			docked: "bottom",
			//title: "Cattle Details",
			//id: 'myTitleC',
			
			
			items: [
			
			
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "index", 
					action: 'blistBack',
					//iconCls: 'reply',
                    //iconMask: true,
				},
				
				
			]
		};
		
		this.add([toolbar,footerbar, { xtype: "vaccinesdetails"}
		
		]);
		

	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		//title: "Cattle Feed",
		iconCls: "address_book",
		iconMask: true
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});