Ext.define("venkys_sale.view.FeedContainer",{
	extend: "Ext.Panel",
	xtype: "feedcontainer",
    requires: ["venkys_sale.view.FeedDetails"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Suppliment Details",
			id: 'feedsupplimentid',
			//id: 'myTitleC',
			
			items: [
			{
					xtype: "button",
					//ui: "back",
					//text: "index", 
					action: 'feedBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'refreshfeedtap',
					//iconCls: 'reply',
                   // iconMask: true,
				},
				
				
			]
		};
		var footerbar = {
			xtype: "toolbar",
			docked: "bottom",
			//title: "Cattle Details",
			//id: 'myTitleC',
			
			
			items: [
			
			
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "index", 
					action: 'blistBack',
					//iconCls: 'reply',
                    //iconMask: true,
				},
				
				
			]
		};
		
		this.add([toolbar,footerbar, { xtype: "feeddetails"}
		
		]);
		

	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		//title: "Cattle Feed",
		iconCls: "address_book",
		iconMask: true
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});