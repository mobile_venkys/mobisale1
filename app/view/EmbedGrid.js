Ext.define("venkys_sale.view.EmbedGrid",{
	extend: "Ext.Panel",
	//extend : 'Ext.ux.touch.grid.View',
	xtype: "embedgrid",
    requires: ["venkys_sale.view.Xprs"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Xprs Details",
			
			
			items: [
			{
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'xpreBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "index", 
					action: 'blistBack',
					//iconCls: 'reply',
                    //iconMask: true,
				},
				
				
			]
		};

		this.add([toolbar, { xtype: "xprs"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true
	
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}

	
});