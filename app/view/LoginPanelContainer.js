Ext.define("venkys_sale.view.LoginPanelContainer",{
	extend: "Ext.Panel",
	xtype: "loginpanelcontainer",
    requires: ["venkys_sale.view.LoginPanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Product List",
			
			
			items: [
			{
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'productlistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				{
					xtype: "button",
					text: "Show Cart",
					
					 html:'<img src="./../Venkys_CRM/resources/images/1.png" width="30px" height="30px" />',
					//badgeText: "0",
					action: 'showcartevent',
					
				}
			]
		};

		this.add([toolbar, { xtype: "loginpanel"}]);
	},

	config: {
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Note List",
		iconCls: "home"
	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});