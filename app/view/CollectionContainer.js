Ext.define("venkys_sale.view.CollectionContainer",{
	extend: "Ext.Panel",
	xtype: "collectioncontainer",
    requires: ["venkys_sale.view.Collection"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Production",
			
			
			items: [
			
			{
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'blistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
				{ xtype: "spacer" },
				
				
			]
		};

		this.add([toolbar, { xtype: "collection"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Cattle Feed",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});