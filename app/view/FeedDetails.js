Ext.define("venkys_sale.view.FeedDetails",{
extend : 'Ext.ux.touch.grid.List',
 xtype: "feeddetails",
 requires : [
        'Ext.ux.touch.grid.feature.Feature',
        'Ext.ux.touch.grid.feature.Editable',
        'Ext.ux.touch.grid.feature.Sorter',
        'Ext.field.Number',
        'venkys_sale.store.FeedSuppliment'
    ],

    config : {
       
		store: 'FeedSuppliment',
	    //url:"http://127.0.0.1/application/Grid-Display/app/php/xpress_details.json",
        //store    : true,
		//url :'http://127.0.0.1/backbone/XpressList.php',
        columns  : [
           {
                header    : 'Material',
                dataIndex : 'Material',
                width     : '25%',
                style    : "text-align: left;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Invoice Qty.(Bags)',
                dataIndex : 'Invoice1',
                width     : '25%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Invoice Qty.(kg)',
                dataIndex : 'Invoice2',
                width     : '25%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            },
			{
                header    : 'Invoice Amount',
                dataIndex : 'Amount',
                width     : '25%',
                style    : "text-align: center;",
				editor    : {
                    xtype  : 'numberfield'
                }
            }
			
			        
        ],
        features : [
            /*{
                ftype    : 'Ext.ux.touch.grid.feature.Sorter',
                launchFn : 'initialize'
            },*/
            {
                ftype    : 'Ext.ux.touch.grid.feature.Editable',
                launchFn : 'initialize'
            }
        ]
    },

	
});