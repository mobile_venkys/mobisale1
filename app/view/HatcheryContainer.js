Ext.define("venkys_sale.view.HatcheryContainer",{
	extend: "Ext.Panel",
	xtype: "hatcherycontainer",
    requires: ["venkys_sale.view.HatcheryPanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Hatchery Plants",
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'blistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				
			]
		};

		this.add([toolbar, { xtype: "hatcherypanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Hatchery",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});