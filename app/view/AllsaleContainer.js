Ext.define("venkys_sale.view.AllsaleContainer",{
	extend: "Ext.Panel",
	xtype: "allsalecontainer",
    requires: ["venkys_sale.view.AllsalePanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Plants",
			id: 'allsaleid',
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'allsaleBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
			
				
			]
		};

		this.add([toolbar, { xtype: "allsalepanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Plants",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});