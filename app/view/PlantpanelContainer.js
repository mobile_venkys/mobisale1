Ext.define("venkys_sale.view.PlantpanelContainer",{
	extend: "Ext.Panel",
	xtype: "plantpanelcontainer",
    requires: ["venkys_sale.view.Plantpanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Feed Plants",
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'blistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
			
				
			]
		};

		this.add([toolbar, { xtype: "plantpanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Cattle Feed",
		iconCls: "home",
		
		

	},

	/*onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}*/
});