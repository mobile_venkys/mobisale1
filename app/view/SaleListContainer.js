Ext.define("venkys_sale.view.SaleListContainer",{
	extend: "Ext.Panel",
	xtype: "salelistcontainer",
    requires: ["venkys_sale.view.SaleList"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Cattle Feed",
			
			
			items: [
			
			{
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'blistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
				{ xtype: "spacer" },
				
				
			]
		};

		this.add([toolbar, { xtype: "salelist"}]);
	},

	config: {
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Cattle Feed",
		iconCls: "home"
	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});