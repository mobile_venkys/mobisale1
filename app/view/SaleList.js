Ext.define("venkys_sale.view.SaleList",{
	//extend: "Ext.form.Panel",
   extend: "Ext.dataview.List",
   xtype: "salelist",
   requires: ["Ext.plugin.ListPaging"],
	store: "Plant",
   // itemId:"loginpanel",

	config: {
	
	store: "Plant",
	store: {
        fields: ['p_id','pl_name'],
        data: [
            {p_id: 1, pl_name: 'Sale'},
            {p_id: 2,pl_name: 'Dispatch'},
			{p_id: 3,pl_name: 'Production'},
			{p_id: 4,pl_name: 'Collection'}
            
        ]
    },
		
           plugins: [
			        {
						xclass: "Ext.plugin.ListPaging",
						autoPaging: false
			        }
		            ],
		 
		itemTpl: [
		'<div class="list">',
		'{pl_name}',
		 ' </div> ',      
                 ],
		
		
		/*onItemTap: function(record,btn,index) 
		{
		     // alert("hiiii 2");
              this.fireEvent("salelistcmd", record,index);
		}*/
		},
	
	
});