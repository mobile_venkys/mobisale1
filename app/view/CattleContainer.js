Ext.define("venkys_sale.view.CattleContainer",{
	extend: "Ext.Panel",
	xtype: "cattlecontainer",
    requires: ["venkys_sale.view.CattlePanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Cattle Plants",
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'blistBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				{ xtype: "spacer" },
				
				
			]
		};

		this.add([toolbar, { xtype: "cattlepanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Cattle Plants",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});