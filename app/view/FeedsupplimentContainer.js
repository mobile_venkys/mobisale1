Ext.define("venkys_sale.view.FeedsupplimentContainer",{
	extend: "Ext.Panel",
	xtype: "feedsupplimentcontainer",
    requires: ["venkys_sale.view.FeedsupplimentPanel"],

	initialize: function() {
	
		var toolbar = {
			xtype: "toolbar",
			docked: "top",
			title: "Trading Details",
			id: 'feedsupplimentid',
			
			
			items: [
			   {
					xtype: "button",
					ui: "back",
					//text: "Back", 
					action: 'feedBack',
					iconCls: 'reply',
                    iconMask: true,
				},
			
				
				
			{ xtype: "spacer" },
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "Refresh", 
					action: 'refreshfeedtap',
					//iconCls: 'reply',
                   // iconMask: true,
				},
				
				
			]
		};
		
		var footerbar = {
			xtype: "toolbar",
			docked: "bottom",
			//title: "Cattle Details",
			//id: 'myTitleC',
			
			
			items: [
			
			
				
				
				{
					xtype: "button",
					//ui: "back",
					text: "index", 
					action: 'blistBack',
					//iconCls: 'reply',
                    //iconMask: true,
				},
				
				
			]
		};

		this.add([toolbar,footerbar, { xtype: "feedsupplimentpanel"}
		
		]);
	},

	config: {
	
		layout: "fit", //critical for list to show
		fullscreen: true,
		title: "Plants",
		iconCls: "home",
		
		

	},

	onAddProductTap: function() {
	
		this.fireEvent("addProductCommand",this);
	}
});