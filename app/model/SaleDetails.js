Ext.define("venkys_sale.model.SaleDetails",{
	extend: "Ext.data.Model",
	config: {
	identifier:'uuid',
		idProperty: "id",
		fields: [
			{ name: "id", type: "integer" },
			{ name: "todays_qty", type: "string" },
			{ name: "todays_amt", type: "string" },
			{ name: "yes_qty", type: "string" },
			{ name: "yes_amt", type: "string" },
			{ name: "avg_monthly", type: "string" },
			{ name: "tar_sale", type: "string" },
			{ name: "mnthly_coll", type: "string" },
			{ name: "coll_till", type: "string" }
           ],
		proxy: {
                type: 'jsonp',
                url: "http://192.168.13.52:36/m/sale1/app/php/SalesTotal.php",
				//callbackKey: 'theCallbackFunction',
				extraParams: {
				p_id: "",
				d_id: ""
			    },
                reader: {
				type: "json",
				rootProperty: "SaleDetails",
				totalProperty: "total"
			}
        },
        autoLoad: true
	}
});