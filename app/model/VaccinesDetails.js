Ext.define("venkys_sale.model.VaccinesDetails",{
	extend: "Ext.data.Model",
	config: {
	identifier:'uuid',
		idProperty: "id",
		fields: [
			       'id',
                   'Country',			   				   
				   'Yesterday',
				   'Weekly',
				   'Monthly'				   		            		
           ],
		 proxy: {
                type: 'ajax',
                url: 'http://192.168.13.52:36/m_login/FinalGrid/php/vaccines.php/',
				//callbackKey: 'theCallbackFunction',
				extraParams: {
				//p_id: "",
				d_id: ""
			   },
                reader: {
                type: 'json',
                rootProperty: 'data'
                }
        },
        autoLoad: true   
	}
});