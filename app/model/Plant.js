Ext.define('venkys_sale.model.Plant',{
    extend: 'Ext.data.Model',
    config: {
	identifier:'uuid',
	//idProperty: "id",
        fields: [
            { name: 'id', type: 'int' },
            { name: 'p_name', type: 'string' }
        ]
    }
});