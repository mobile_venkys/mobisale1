Ext.define("venkys_sale.model.FeedSuppliment",{
	extend: "Ext.data.Model",
	config: {
	identifier:'uuid',
		idProperty: "id",
		fields: [
			       'id',
                   'Material',			   
				   'Invoice1',
				   'Invoice2',
				   'Amount'				   		            		
           ],
		 proxy: {
                type: 'jsonp',
                url: 'http://192.168.13.52:36/m_login/FinalGrid/php/suppliment.php/',
				//callbackKey: 'theCallbackFunction',
				extraParams: {
				//p_id: "",
				d_id: ""
			   },
                reader: {
                type: 'json',
                rootProperty: 'data'
                }
        },
        autoLoad: true   
	}
});