Ext.define('venkys_sale.model.Biscuit',{
    extend: 'Ext.data.Model',
    config: {
	identifier:'uuid',
	idProperty: "bid",
        fields: [
            { name: 'bid', type: 'int' },
            { name: 'b_name', type: 'string' }
        ]
    }
});