Ext.define('venkys_sale.model.Integration',{
    extend: 'Ext.data.Model',
    config: {
	identifier:'uuid',
	idProperty: "iid",
        fields: [
            { name: 'iid', type: 'int' },
            { name: 'i_name', type: 'string' }
        ]
    }
});