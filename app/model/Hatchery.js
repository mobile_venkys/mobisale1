Ext.define('venkys_sale.model.Hatchery',{
    extend: 'Ext.data.Model',
    config: {
	identifier:'uuid',
	idProperty: "hid",
        fields: [
            { name: 'hid', type: 'int' },
            { name: 'h_name', type: 'string' }
        ]
    }
});