Ext.define("venkys_sale.model.Cattlefeed",{
	extend: "Ext.data.Model",
	config: {
	identifier:'uuid',
		idProperty: "id",
		fields: [
			       'id',
                   'Details',			   
				   'Today',
				   'Yesterday',
				   'Monthly'				   		            		
           ],
		 proxy: {
                type: 'jsonp',
                url: 'http://192.168.13.52:36/m_login/FinalGrid/php/Cattlegrid.php/',
				//callbackKey: 'theCallbackFunction',
				extraParams: {
				p_id: "",
				d_id: ""
			   },
                reader: {
                type: 'json',
                rootProperty: 'data'
                }
        },
        autoLoad: true   
	}
});