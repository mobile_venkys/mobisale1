Ext.define('venkys_sale.model.Cattle',{
    extend: 'Ext.data.Model',
    config: {
	identifier:'uuid',
	idProperty: "cid",
        fields: [
            { name: 'cid', type: 'int' },
            { name: 'c_name', type: 'string' }
        ]
    }
});