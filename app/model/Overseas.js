Ext.define('venkys_sale.model.Overseas',{
    extend: 'Ext.data.Model',
    config: {
	identifier:'uuid',
	//idProperty: "id",
        fields: [
            { name: 'oid', type: 'int' },
            { name: 'o_name', type: 'string' }
        ]
    }
});