Ext.define("venkys_sale.model.BiscuitList",{
	extend: "Ext.data.Model",
	config: {
	identifier:'uuid',
		idProperty: "id",
		fields: [
			       'id',
                   'Details',
				   'Result'				   		           		
           ],
		   proxy: {
                type: 'jsonp',
                url: 'http://192.168.13.52:36/m_login/FinalGrid/php/Biscuitgrid.php/',
				//callbackKey: 'theCallbackFunction',
				extraParams: {
				p_id: "",
				d_id: ""
			   },
                reader: {
                type: 'json',
                rootProperty: 'data'
                }
        },
        autoLoad: true  
		   
	}
});