Ext.define('venkys_sale.model.Division',{
    extend: 'Ext.data.Model',
    config: {
	identifier:'uuid',
	idProperty: "id",
        fields: [
            { name: 'id', type: 'int' },
            { name: 'd_name', type: 'string' }
        ]
    }
});