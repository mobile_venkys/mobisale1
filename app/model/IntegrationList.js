Ext.define("venkys_sale.model.IntegrationList",{
	extend: "Ext.data.Model",
	config: {
	identifier:'uuid',
		idProperty: "id",
		fields: [
			       'id',
                   'Company',
				   'Yesterday',
				   'Byesterday',
				   'Bbyesterday'
				   /*'Today',
				   'Yesterday',
				   'Monthly',
				   'Avg',
				   'Target'*/					
           ],
		    proxy: {
                type: 'jsonp',
                url: 'http://192.168.13.52:36/m_login/FinalGrid/php/Integrationgrid.php/',
				//callbackKey: 'theCallbackFunction',
				extraParams: {
				p_id: "",
				d_id: ""
			   },
                reader: {
                type: 'json',
                rootProperty: 'IntegrationList'
                }
        },
        autoLoad: true  
		
	}
});