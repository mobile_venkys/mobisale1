Ext.define("venkys_sale.store.Overseas",{
	extend: "Ext.data.Store",

	requires: ["venkys_sale.model.Overseas"],
   // sorters: ['b_name', 'b_id'],
	config: {
		model: "venkys_sale.model.Overseas",
		proxy: {
			type: "ajax",
			url: "plant.json",
			reader: {
			  type:'json',
			  rootProperty:'oversease'}
			
		},
		
		//pageSize: 1,
		autoLoad: true
	}
});