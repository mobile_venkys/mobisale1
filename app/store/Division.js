Ext.define("venkys_sale.store.Division",{
	extend: "Ext.data.Store",

	requires: ["venkys_sale.model.Division"],
    // sorters: ['b_name', 'b_id'],
	config: {
		     model: "venkys_sale.model.Division",
		     proxy: {
			         type: "ajax",
			         url: "division.json",
			           reader: {
			                   type:'json',
			                   rootProperty:'div'}
			
		},
		
		//pageSize: 1,
		autoLoad: true
	}
});