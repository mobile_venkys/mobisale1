Ext.define("venkys_sale.store.Hatchery",{
	extend: "Ext.data.Store",

	requires: ["venkys_sale.model.Hatchery"],
   // sorters: ['b_name', 'b_id'],
	config: {
		model: "venkys_sale.model.Hatchery",
		proxy: {
			type: "ajax",
			url: "plant.json",
			reader: {
			  type:'json',
			  rootProperty:'hatchery'}
			
		},
		
		//pageSize: 1,
		autoLoad: true
	}
});