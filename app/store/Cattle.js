Ext.define("venkys_sale.store.Cattle",{
	extend: "Ext.data.Store",

	requires: ["venkys_sale.model.Cattle"],
   // sorters: ['b_name', 'b_id'],
	config: {
		model: "venkys_sale.model.Cattle",
		proxy: {
			type: "ajax",
			url: "plant.json",
			reader: {
			  type:'json',
			  rootProperty:'cattle'}
			
		},
		
		//pageSize: 1,
		autoLoad: true
	}
});