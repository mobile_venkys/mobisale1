Ext.define("venkys_sale.store.Integration",{
	extend: "Ext.data.Store",

	requires: ["venkys_sale.model.Integration"],
   // sorters: ['b_name', 'b_id'],
	config: {
		model: "venkys_sale.model.Integration",
		proxy: {
			type: "ajax",
			url: "plant.json",
			reader: {
			  type:'json',
			  rootProperty:'integration'}
			
		},
		
		//pageSize: 1,
		autoLoad: true
	}
});