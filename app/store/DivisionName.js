Ext.define('venkys_sale.store.DivisionName',{
    extend: 'Ext.data.Store',
     
    config: {
     model: 'venkys_sale.model.DivisionName',
	//  storeId: "myStoreID",
     autoLoad: true,
       
     proxy: {
      //use sessionstorage if need to save data for that
      //specific session only
       type: 'localstorage',
       id  : 'd_id'
     }
    }
});
