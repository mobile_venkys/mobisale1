Ext.define("venkys_sale.store.Plant",{
	extend: "Ext.data.Store",

	requires: ["venkys_sale.model.Plant"],
   // sorters: ['b_name', 'b_id'],
	config: {
		model: "venkys_sale.model.Plant",
		proxy: {
			type: "ajax",
			url: "plant.json",
			reader: {
			  type:'json',
			  rootProperty:'plant'}
			
		},
		
		//pageSize: 1,
		autoLoad: true
	}
});