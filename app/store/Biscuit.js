Ext.define("venkys_sale.store.Biscuit",{
	extend: "Ext.data.Store",

	requires: ["venkys_sale.model.Biscuit"],
   // sorters: ['b_name', 'b_id'],
	config: {
		model: "venkys_sale.model.Biscuit",
		proxy: {
			type: "ajax",
			url: "plant.json",
			reader: {
			  type:'json',
			  rootProperty:'biscuit'}
			
		},
		
		//pageSize: 1,
		autoLoad: true
	}
});