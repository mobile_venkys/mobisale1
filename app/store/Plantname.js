Ext.define('venkys_sale.store.Plantname',{
    extend: 'Ext.data.Store',
     
    config: {
     model: 'venkys_sale.model.Plantname',
	//  storeId: "myStoreID",
     autoLoad: true,
       
     proxy: {
      //use sessionstorage if need to save data for that
      //specific session only
       type: 'localstorage',
       id  : 'plant_id'
     }
    }
});
