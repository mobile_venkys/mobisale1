Ext.define("venkys_sale.controller.Feed",{
	extend: "Ext.app.Controller",
	
	config: {
		
		refs: {
		    mainPanel : "mainpanel",  // for main page tabbar
			homePanel : "homePanel",  // for the home page main panel button ie. new transaction etc
			plantPanel:"plantpanel",
            plantpanelContainer:"plantpanelcontainer",			// for login panel
            saleList:"salelist",
            salelistContainer:"salelistcontainer",
            saleDetails:"saledetails",
            saledetailsContainer:"saledetailscontainer",
            dispatch:"dispatch",
            dispatchContainer:"dispatchcontainer",
            collection:"collection",
            collectionContainer:"collectioncontainer",
            collector:"collector",
            collectorPanel:"collectorpanel",
            divisionPanel:"divisionpanel",
            divisionContainer:"divisioncontainer",
            cattlePanel:"cattlepanel",
            cattleContainer:"cattlecontainer",
            integrationPanel:"integrationpanel",
            integrationContainer:"integrationcontainer",
            hatcheryPanel:"hatcherypanel",
            hatcheryContainer:"hatcherycontainer",
            biscuitPanel:"biscuitpanel",
            biscuitContainer:"biscuitcontainer",
            cattleFeed:"cattlefeed",
            cattlefeedContainer:"cattlefeedcontainer",
            hatcheryList:"hatcherylist",			
			hatcherylistContainer:"hatcherylistcontainer",
			integrationList:"integrationlist",
			integrationlistPanel:"integrationlistpanel",
			biscuitList:"biscuitlist",
			biscuitlistContainer:"biscuitlistcontainer",
			overseasPanel:"overseaspanel",
			overseaspanelContainer:"overseaspanelcontainer",
			overseasDetails:"overseasdetails",
			overseasdetailsContainer:"overseasdetailscontainer",
			allPanel:"allpanel",
			allContainer:"allcontainer",
			allsaleContainer: "allsalecontainer",
			allsalePanel: "allsalepanel",
			alldispatchPanel:"alldispatchpanel",
			alldispatchContainer:"alldispatchcontainer",
			allcollPanel:"allcollpanel",
			allcollContainer:"allcollcontainer",
			liveSale:"livesale",
			livesaleContainer:"livesalecontainer",
			liveDispatch:"livedispatch",
			livedispatchContainer:"livedispatchcontainer",
			allpayPanel:"allpaypanel",
			allpayContainer:"allpaycontainer",
			xprsContainer:"xprscontainer",
			xprsDetails:"xprsdetails",
			feedContainer:"feedcontainer",
			feedDetails:"feeddetails",
			vaccinesContainer:"vaccinescontainer",
			vaccinesDetails:"vaccinesdetails",
			tradeContainer:"tradecontainer",
			tradePanel:"tradepanel"
			//embedGrid:"embedgrid",
			
			
	
		},
  
		control: {
		
		    allPanel:{
		    itemtap:'onAllpanel'
		   },
				
			plantpanel: {
				SaleCommand: "onSale",
				itemtap: 'onItemTap_plant'
			},
			cattlePanel: {
				
				itemtap: 'onItemTap_cattle'
			},
			integrationPanel: {
				
				itemtap: 'onItemTap_integration'
			},
			hatcheryPanel: {
				
				itemtap: 'onItemTap_hatchery'
			},
			biscuitPanel: {
				
				itemtap: 'onItemTap_biscuit'
			},
			IntegrationList: {
				
				itemtap: 'onItemTap_intelist'
			},
			saleList: {
				salelistcmd: "onSaleList",
				itemtap: 'onItemTap'
			},
			overseasPanel: {
				
				itemtap: 'onItemTap_overseas'
			},
			allPanel: {
				
				itemtap: 'onItemTap_allsale'
			},
			tradePanel: {
				
				itemtap: 'onItemTap_trade'
			},			
			
	       divisionpanel: {
				
				itemtap: 'onItemTap_div'
			},
			
		"button[action=refreshlivedispatchtap]":{
            tap: 'onrefreshlivedispatchtap'
        },
			"button[action=refreshlivesaletap]":{
            tap: 'onrefreshlivesaletap'
        },
			"button[action=refreshallpaytap]":{
            tap: 'onrefreshallpaytap'
        },
			"button[action=refreshallcolltap]":{
            tap: 'onrefreshallcolltap'
        },
			"button[action=refreshalldispatchtap]":{
            tap: 'onrefreshalldispatchtap'
        },	
			"button[action=refreshcattletap]":{
            tap: 'onrefreshcattletap'
        },
			"button[action=refreshhatcherytap]":{
            tap: 'onrefreshhatcherytap'
        },
			"button[action=refreshbiscuittap]":{
            tap: 'onrefreshbiscuittap'
        },	
			"button[action=onrefreshovertap]":{
            tap: 'onrefreshovertap'
        },
		
		   "button[action=refreshxprstap]":{
            tap: 'onrefreshxprstap'
        },
		   "button[action=refreshfeedtap]":{
            tap: 'onrefreshfeedtap'
        },
		   "button[action=refreshvaccinestap]":{
            tap: 'onrefreshvaccinestap'
        },
		
		   "button[action=refreshtap]":{
		   tap:'onRefreshtap'
		},
		
		 "button[action=allsaleBack]": {
            tap: 'onallsaleBacktap'
        },
		
		"button[action=alldispatchBack]": {
            tap: 'onalldispatchBacktap'
        },
		
		"button[action=allcollBack]": {
            tap: 'onallcollBacktap'
        },	
			"button[action=blistBack]": {
            tap: 'onblistBacktap'
        },
		
		
		"button[action=plantBack]": {
            tap: 'onplantBacktap'
        },
		
		"button[action=cplantBack]": {
            tap: 'oncplantBacktap'
        },
		"button[action=xpreBack]": {
            tap: 'onxpreBacktap'
        },
		"button[action=bisBack]": {
            tap: 'onBisBack'
        },
		"button[action=inteback]": {
            tap: 'onInteback'
        },
		"button[action=hatchBack]": {
            tap: 'onHatchBack'
        },
		"button[action=overBack]": {
            tap: 'onOverBack'
        },
		"button[action=feedBack]": {
            tap: 'onfeedBack'
        },
		"button[action=vaccinesBack]": {
            tap: 'onvaccinesBack'
        },
		
		
			
		}
	},
	//======================for division tap==============================
	onItemTap_div: function(view, index, target, record, event){
	                var store = Ext.getStore('DivisionName');
                     store.getProxy().clear();
                     store.load();
		                        
		                          var index;
								  
                    
                   var store1 = Ext.getStore('DivisionName'); // Get the store
                   store1.add({d_id:index}); // Add an instance of you model item
                   store1.sync(); // Will add
				  if(index==0)
				  {
		           Ext.Viewport.animateActiveItem(this.getPlantpanelContainer(), { type: "slide", direction: "left" });
				     //var store = Ext.getStore('Plantname');
                     //store.getProxy().clear();
                     //store.load();
				 }
				 else if(index==1)
				 {
				    
					Ext.Viewport.animateActiveItem(this.getCattleContainer(), { type: "slide", direction: "left" });
				 }
				  else if(index==2)
				 {
				    Ext.Viewport.animateActiveItem(this.getIntegrationContainer(), { type: "slide", direction: "left" });
				 }
				  else if(index==3)
				 {
				    Ext.Viewport.animateActiveItem(this.getBiscuitContainer(), { type: "slide", direction: "left" });
				 }
				 else if(index==4)
				 {
				    Ext.Viewport.animateActiveItem(this.getOverseaspanelContainer(), { type: "slide", direction: "left" });
				 }
				  else if(index==5)
				 {
				   
				    Ext.Viewport.animateActiveItem(this.getAllContainer(), { type: "slide", direction: "left" });
				 }
				 else if(index==6)
				 {
				   				    
		         var record2 = Ext.getStore("DivisionName").getAt(0);
                 var d_id=record2.get('d_id');
							  	
	             var store = Ext.getStore("XprsDetails");		       
		         store.getProxy().setExtraParam("d_id",d_id);
		         store.loadPage(1);
                 Ext.Viewport.animateActiveItem(this.getXprsContainer(), { type: "slide", direction: "left" });
				 
				 }				 
				 else if(index==7)
				 {
				   				    
		         Ext.Viewport.animateActiveItem(this.getTradeContainer(), { type: "slide", direction: "left" });
				 
				 }
				 
				 
				  
	}, 

	
	
	onItemTap_cattle: function(view, index, target, record, event){
	
		                        
		                          var index;
								  
			 if(index==0)
		{
		//alert(index);
		Ext.getCmp('myTitleC').setTitle('Khanna');
		}
		else if(index==1)
		{
		//alert(plant_id);
		Ext.getCmp('myTitleC').setTitle('Ketkawale');
		}					  
								                   
                   var store1 = Ext.getStore('Plantname'); // Get the store
                   store1.add({plant_id:index}); // Add an instance of you model item
                   store1.sync(); // Will add
				
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
							  
	
	    var store = Ext.getStore("Cattlefeed");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
        Ext.Viewport.animateActiveItem(this.getCattlefeedContainer(), { type: "slide", direction: "right" });
			  
	},
	  
	  onItemTap_allsale: function(view, index, target, record, event){
	      
		
		                       var index;
							   //alert(index);
								  
                
		
		/*if(index==0)
		{
		
		//alert(plant_id);
		Ext.getCmp('allsaleid').setTitle('Sale');
		  Ext.Viewport.animateActiveItem(this.getAllsaleContainer(), { type: "slide", direction: "right" });
		}*/
	    if(index==0)
		{
		
		//alert(plant_id);
		 var store = Ext.getStore("Salesdispatch");
		 store.loadPage(1);
		 Ext.getCmp('alldispatchsaleid').setTitle('Dispatch');
		 Ext.Viewport.animateActiveItem(this.getAlldispatchContainer(), { type: "slide", direction: "right" });
		}
		else if(index==1)
		{
		var store = Ext.getStore("Coll_pay");
		 store.loadPage(1);
		Ext.getCmp('allcollectionid').setTitle('Collection');
		 Ext.Viewport.animateActiveItem(this.getAllcollContainer(), { type: "slide", direction: "right" });
		}
		
		else if(index==2)
		{
		var store = Ext.getStore("All_pay");
		 store.loadPage(1);
		Ext.getCmp('allpayid').setTitle('Payment');
		 Ext.Viewport.animateActiveItem(this.getAllpayContainer(), { type: "slide", direction: "right" });
		}
		
		else if(index==3)
		{
		var store = Ext.getStore("Live_sale");
		 store.loadPage(1);
		Ext.getCmp('alllivesaleid').setTitle('Live Sale');
		 Ext.Viewport.animateActiveItem(this.getLivesaleContainer(), { type: "slide", direction: "right" });
		}
		
		else if(index==4)
		{
		 var store = Ext.getStore("Live_dispatch");
		  store.loadPage(1);
		  Ext.getCmp('alllivedispatchid').setTitle('Live Dispatch');
		   Ext.Viewport.animateActiveItem(this.getLivedispatchContainer(), { type: "slide", direction: "right" });
	
		 
		}
		
		 
	}, 
			
	
	//**********overseas************
     	onItemTap_overseas: function(view, index, target, record, event){
	      
		                          var index;
								  
                    
                   var store1 = Ext.getStore('Plantname'); // Get the store
                   store1.add({plant_id:index}); // Add an instance of you model item
                   store1.sync(); // Will add
				
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
							  
	
	    var store = Ext.getStore("OverseasDetails");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
        Ext.Viewport.animateActiveItem(this.getOverseasdetailsContainer(), { type: "slide", direction: "right" });
			  
	}, 
	
	
	//***********end of overseas***********************
	//===================================end of division tap============================
	onItemTap_integration:function(view, index, target, record, event){
	//alert(index);
	
		if(index==0)
		{
		
               var index;
               var store1 = Ext.getStore('Plantname'); // Get the store
               store1.add({plant_id:index}); // Add an instance of you model item
               store1.sync(); // Will add
				
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
		var store = Ext.getStore("HatcheryList");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
		
		Ext.getCmp('myTitleHatch').setTitle('Kolkata');
		Ext.Viewport.animateActiveItem(this.getHatcherylistContainer(), { type: "slide", direction: "right" });
		}
		if(index==1)
		{
		
		          var index;
								  
                    
                   var store1 = Ext.getStore('Plantname'); // Get the store
                   store1.add({plant_id:index}); // Add an instance of you model item
                   store1.sync(); // Will add
				//store.getProxy().clear();
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
		var store = Ext.getStore("HatcheryList");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
		//alert(plant_id);
		Ext.getCmp('myTitleHatch').setTitle('Siliguri');
		Ext.Viewport.animateActiveItem(this.getHatcherylistContainer(), { type: "slide", direction: "right" });
		}
		if(index==2)
		{
		
		          var index;
								  
                    
                   var store1 = Ext.getStore('Plantname'); // Get the store
                   store1.add({plant_id:index}); // Add an instance of you model item
                   store1.sync(); // Will add
				//store.getProxy().clear();
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
		var store = Ext.getStore("HatcheryList");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
		//alert(plant_id);
		Ext.getCmp('myTitleHatch').setTitle('Guwahati');
		Ext.Viewport.animateActiveItem(this.getHatcherylistContainer(), { type: "slide", direction: "right" });
		}
		if(index==3)
		{
		
		          var index;
                  var store1 = Ext.getStore('Plantname'); // Get the store
                  store1.add({plant_id:index}); // Add an instance of you model item
                  store1.sync(); // Will add
				//store.getProxy().clear();
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
		var store = Ext.getStore("IntegrationList");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
		//alert(plant_id);
		Ext.getCmp('myTitleInte').setTitle('Hyderabad');
		Ext.Viewport.animateActiveItem(this.getIntegrationlistPanel(), { type: "slide", direction: "right" });
		}
		
        
			  
	}, 
	onItemTap_biscuit:function(view, index, target, record, event){
	
		                        
		                          var index;
								  
                    
                   var store1 = Ext.getStore('Plantname'); // Get the store
                   store1.add({plant_id:index}); // Add an instance of you model item
                   store1.sync(); // Will add
				
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
							  
	
	    var store = Ext.getStore("BiscuitList");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
		
        if(index==0)
		{
		//alert(plant_id);
		Ext.getCmp('myTitleBisc').setTitle('Biscuit');
		}
		else if(index==1)
		{
		//alert(plant_id);
		Ext.getCmp('myTitleBisc').setTitle('Atta');
		}
        Ext.Viewport.animateActiveItem(this.getBiscuitlistContainer(), { type: "slide", direction: "right" });
			  
	},  	
	
	onItemTap_plant: function(view, index, target, record, event){
	
		       //var plant = Ext.create('venkys_sale.view.SaleDetails');
               //this.getFeed().push(plant);
                               
		                          var index;
								  
                    
                   var store1 = Ext.getStore('Plantname'); // Get the store
                   store1.add({plant_id:index}); // Add an instance of you model item
                   store1.sync(); // Will add
				  
		         
				   
				   var index;
				   
			   
	//alert(index);
	 //var rec = view.getStore().getAt(index);
	 //alert(rec);
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		//alert(index);
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
							  
	    //var group_store= Ext.getStore("All");
	    //var business=serverUrl+"php/All.php";
		
		/*var business=serverUrl+"php/A.php";
        var store = Ext.getStore("All");
        store.getProxy().setUrl(business);
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);*/
		
        /*group_store.getProxy().setUrl("http://192.168.13.52:36/m_login/FinalGrid/php/All.php");    
        group_store.getProxy().setExtraParam("p_id",plant_id);
		group_store.getProxy().setExtraParam("d_id",d_id);
		group_store.load();
		plant.setStore(group_store);*/
		
		//your_list.setStore(All);		   
	    var store = Ext.getStore("All");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
		if(index==0)
		{
		//alert(plant_id);
		Ext.getCmp('myTitle').setTitle('Ketkawale');
		}
		else if(index==1)
		{
		//alert(plant_id);
		Ext.getCmp('myTitle').setTitle('Kurnool');
		}
		else if(index==2)
		{
		//alert(plant_id);
		Ext.getCmp('myTitle').setTitle('Ajmer');
		}
		else if(index==3)
		{
		//alert(plant_id);
		Ext.getCmp('myTitle').setTitle('Khanna');
		}
		else if(index==4)
		{
		//alert(plant_id);
		Ext.getCmp('myTitle').setTitle('Namakkal');
		}
		else if(index==5)
		{
		//alert(plant_id);
		Ext.getCmp('myTitle').setTitle('Varanasi');
		}
		 
		 //plant.setData(record.data);
		 
         Ext.Viewport.animateActiveItem(this.getSaledetailsContainer(), { type: "slide", direction: "right" });
			  
	}, 
	
	onItemTap_trade: function(view, index, target, record, event){
	      
		
		                       var index;
							   //alert(index);
								  
                
		
		/*if(index==0)
		{
		
		//alert(plant_id);
		Ext.getCmp('allsaleid').setTitle('Sale');
		  Ext.Viewport.animateActiveItem(this.getAllsaleContainer(), { type: "slide", direction: "right" });
		}*/
	    if(index==0)
		{
		
		//alert(plant_id);
		 var store = Ext.getStore("FeedSuppliment");
		 store.loadPage(1);
		 Ext.getCmp('feedsupplimentid').setTitle('Suppliment Details');
		 Ext.Viewport.animateActiveItem(this.getFeedContainer(), { type: "slide", direction: "right" });
		}
		else if(index==1)
		{
		
		 var store = Ext.getStore("VaccinesDetails");
		 store.loadPage(1);
		 Ext.getCmp('vaccinesid').setTitle('Vaccines Details');
		 Ext.Viewport.animateActiveItem(this.getVaccinesContainer(), { type: "slide", direction: "right" });
		}
				
		 
	}, 
    onrefreshcattletap:function(){
	var index;
	var store1 = Ext.getStore('Plantname'); // Get the store
                   store1.add({plant_id:index}); // Add an instance of you model item
                   store1.sync(); // Will add
				
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
							  
	
	    var store = Ext.getStore("Cattlefeed");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
	
	},
	onrefreshbiscuittap:function(){
	var index;
         var store1 = Ext.getStore('Plantname'); // Get the store
         store1.add({plant_id:index}); // Add an instance of you model item
         store1.sync(); // Will add
				
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
							  
	
	    var store = Ext.getStore("BiscuitList");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
	},
	onrefreshhatcherytap:function(){
	 var index;
								  
                    
                   var store1 = Ext.getStore('Plantname'); // Get the store
                   store1.add({plant_id:index}); // Add an instance of you model item
                   store1.sync(); // Will add
				
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
							  
	
	    var store = Ext.getStore("HatcheryList");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
	
	
	},
	onrefreshovertap:function(){
	 var index;
								  
                    
                   var store1 = Ext.getStore('Plantname'); // Get the store
                   store1.add({plant_id:index}); // Add an instance of you model item
                   store1.sync(); // Will add
				
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
							  
	
	    var store = Ext.getStore("OverseasDetails");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
	
	
	},
	onrefreshxprstap:function(){
	 var index;
	 
	    var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
		
	    var store = Ext.getStore("XprsDetails");		
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
	
	
	},
	
	onrefreshfeedtap:function(){
	 			   
	    var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
		
	    var store = Ext.getStore("FeedSuppliment");		
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
		//Ext.Viewport.animateActiveItem(this.getFeedContainer(), { type: "slide", direction: "left" });
	
	
	},
	onrefreshvaccinestap:function(){	
				   
	    var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
		
	    var store = Ext.getStore("VaccinesDetails");		
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
	   //Ext.Viewport.animateActiveItem(this.getVaccinesContainer(), { type: "slide", direction: "left" });
	
	},
	onRefreshtap:function(){
		var index;
								  
                    
                   var store1 = Ext.getStore('Plantname'); // Get the store
                   store1.add({plant_id:index}); // Add an instance of you model item
                   store1.sync(); // Will add
				  
		           var index;
	
	 //alert(rec);
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		//alert(index);
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
							  
	
	    var store = Ext.getStore("All");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
		
	},
	
	onrefreshlivedispatchtap:function(){
      var store = Ext.getStore("Live_dispatch");
		
		store.loadPage(1);
	
	},
	onrefreshlivesaletap:function(){
      var store = Ext.getStore("Live_sale");
		
		store.loadPage(1);
	
	},
	onrefreshallpaytap:function(){
      var store = Ext.getStore("All_pay");
		
		store.loadPage(1);
	
	},
	onrefreshallcolltap:function(){
      var store = Ext.getStore("Coll_pay");
		
		store.loadPage(1);
	
	},
	onrefreshalldispatchtap:function(){
      var store = Ext.getStore("Salesdispatch");
		
		store.loadPage(1);
	
	},
	onblistBacktap: function() {
	   var store = Ext.getStore('Plantname');
                     store.getProxy().clear();
                     store.load();
					 
		Ext.Viewport.animateActiveItem(this.getMainPanel(), { type: "slide", direction: "right" });
		//this.getBussinessSearch().reset();
	},
	
	
	onplantBacktap: function() {
	var index;
	 var store = Ext.getStore('Plantname');
                     store.getProxy().clear();
                     store.load();
					 
		Ext.Viewport.animateActiveItem(this.getPlantpanelContainer(), { type: "slide", direction: "right" });
		
		
	},
	
	
	oncplantBacktap: function() {
	
	
	 var store = Ext.getStore('Plantname');
                     store.getProxy().clear();
                     store.load();
					  
		Ext.Viewport.animateActiveItem(this.getCattleContainer(), { type: "slide", direction: "right" });
		
		
	},
	onBisBack:function() {
	
	var index;
	 var store = Ext.getStore('Plantname');
                     store.getProxy().clear();
                     store.load();
					 
		Ext.Viewport.animateActiveItem(this.getBiscuitContainer(), { type: "slide", direction: "right" });
		
		
	},
	onInteback: function() {
	
	var index;
	 var store = Ext.getStore('Plantname');
                     store.getProxy().clear();
                     store.load();
					 
		Ext.Viewport.animateActiveItem(this.getIntegrationContainer(), { type: "slide", direction: "right" });
		
		
	},
	onHatchBack:function() {
	
	var index;
	 var store = Ext.getStore('Plantname');
                     store.getProxy().clear();
                     store.load();
					 
		Ext.Viewport.animateActiveItem(this.getIntegrationContainer(), { type: "slide", direction: "right" });
		
		
	},
	onOverBack:function() {
	
	var index;
	 var store = Ext.getStore('Plantname');
                     store.getProxy().clear();
                     store.load();
					 
		Ext.Viewport.animateActiveItem(this.getOverseaspanelContainer(), { type: "slide", direction: "right" });
		
		
	},
		onallsaleBacktap: function() {
	   
					 
		Ext.Viewport.animateActiveItem(this.getAllContainer(), { type: "slide", direction: "right" });
		//this.getBussinessSearch().reset();
	},
	
	onalldispatchBacktap: function() {
	   
					 
		Ext.Viewport.animateActiveItem(this.getAllContainer(), { type: "slide", direction: "right" });
		//this.getBussinessSearch().reset();
	},
	
	
	onallcollBacktap: function() {
	   
					 
		Ext.Viewport.animateActiveItem(this.getAllContainer(), { type: "slide", direction: "right" });
		//this.getBussinessSearch().reset();
	},
	onfeedBack: function() {
	   
					 
		Ext.Viewport.animateActiveItem(this.getTradeContainer(), { type: "slide", direction: "right" });
		//this.getBussinessSearch().reset();
	},
	onvaccinesBack: function() {
	   
					 
		Ext.Viewport.animateActiveItem(this.getTradeContainer(), { type: "slide", direction: "right" });
		//this.getBussinessSearch().reset();
	},
	onxpreBacktap: function() {
	//alert("hiii");
	var index;
	 var store = Ext.getStore('Plantname');
                     store.getProxy().clear();
                     store.load();
					 
		Ext.Viewport.animateActiveItem(this.getXpressContainer(), { type: "slide", direction: "right" });
		
		
	},
	
	onItemTap: function(view, index, target, record, event) {
	var index;
	//alert(index);
	 //var rec = view.getStore().getAt(index);
	 //alert(rec);
	    var record1 = Ext.getStore("Plantname").getAt(0);
        var plant_id=record1.get('plant_id');
		var record2 = Ext.getStore("DivisionName").getAt(0);
        var d_id=record2.get('d_id');
		
							  
	    if(d_id == 0){
	    var store = Ext.getStore("All");
		store.getProxy().setExtraParam("p_id",plant_id);
		store.getProxy().setExtraParam("d_id",d_id);
		store.loadPage(1);
        Ext.Viewport.animateActiveItem(this.getSaledetailsContainer(), { type: "slide", direction: "right" });
		}
		
								  
	
   //==========================cattle division=========================

	
}, 


});// end of controller
