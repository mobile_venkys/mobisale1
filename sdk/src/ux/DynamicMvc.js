/**
* Script name:	DynamicMvc
* 
* Description:	This script will dynamically load controllers and views files;
* 				Full description can be found here:
* 				http://www.onlinesolutionsdevelopment.com/blog/web-development/javascript/loading-sencha-touch-controllers-and-view-files-dynamically/
* 
* Author:		CAM
* Author URL:	http://www.onlinesolutionsdevelopment.com/blog/
*/

Ext.ns('Ext.ux.DynamicMvc');

Ext.ux.DynamicMvc.init = function()
{
	// before dispatch
	Ext.Dispatcher.on('before-dispatch', function(interaction)
	{
		var route = interaction.historyUrl;
		//
		if ( ! route)
		{
			return true;
		}
		//
		this.dispatchOptions = Ext.Router.recognize(route);
		this.dispatchOptions = Ext.applyIf(this.dispatchOptions, interaction);
		//
		var controller = this.dispatchOptions.controller;
		var action = interaction.action;
		//
		var actionView = this.getViewName(controller, action);
		
		// Check the existence of the view
		if ( ! Ext.ComponentMgr.isRegistered(actionView))
		{
			var actionViewFile = this.getViewFile(actionView, controller);
			
			// load view file
			this.loadScriptFile(actionViewFile, function()
			{
				// Check the existence of the controller
				if ( ! interaction.controller)
				{
					this.loadControllerFile(controller);
				}
				else // dispatch
				{
					Ext.dispatch(this.dispatchOptions);
				}
			}, this);
			
			return false;
		}
		// Check the existence of the controller
		else if ( ! interaction.controller)
		{
			this.loadControllerFile(controller);
			
			return false;
		}
	}, this);
}

/**
 * Helper functions.
 */
Ext.ux.DynamicMvc.getViewName = function(controller, action)
{
	return controller + action.charAt(0).toUpperCase() + action.substr(1);
}
/**/
Ext.ux.DynamicMvc.getViewFile = function(viewName, controller)
{
	return 'app/views/'+ controller.toLowerCase() + '/' + viewName + 'View.js';
}
/**/
Ext.ux.DynamicMvc.getControllerFile = function(controller)
{
	return 'app/controllers/'+ controller +'Controller.js';
}

/**
 * Loads the controller file.
 */
Ext.ux.DynamicMvc.loadControllerFile = function(controller)
{
	var controllerFile = this.getControllerFile(controller);
	
	this.loadScriptFile(controllerFile, function(){
		Ext.dispatch(this.dispatchOptions);
	}, this);
}

/**
 * Loads the script file.
 */
Ext.ux.DynamicMvc.loadScriptFile = function(url, callback, scope)
{
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    script.onload = function(){
    	callback.call(scope);
    };

    document.getElementsByTagName('head')[0].appendChild(script);
}

Ext.ux.DynamicMvc.init();